#! /urs/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py ruta
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2023
# -----------------------------

import sys
import argparse
from subprocess import Popen, PIPE
parser =  argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("ruta", type=str,\
        help="directori a llistar")
args=parser.parse_args()
print(args)
#----------------
command = [ "ls", args.ruta ]
pipeData = Popen(command, stdout = PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit(0)
