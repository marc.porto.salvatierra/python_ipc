# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,socket

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT)) # es connecta
s.listen(1)
conn, addr = s.accept() # accepta l'IP:port i la connexio
print("Conn:", type(conn), conn)
print("Connected: ", addr)
while True:
    data = conn.recv(1024)
    conn.send(data)
conn.close()
sys.exit(0)
