#! /urs/bin/python
#-*- coding: utf-8-*-
#
# head [file]
# 10 lines, file o stdin
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2022
# -----------------------------

import sys

fileIn=sys.stdin
MAXLIN=10
if len(sys.argv)==2:
    fileIn=open(sys.argv[1],"r")
counter=0

for line in fileIn:
    counter +=1
    print(line, end="") # end="" -> Serveix per que no faci salt de linea

    if counter==MAXLIN: break

fileIn.close()
exit(0)

# TextIOWrapper -> Text Input Output Wrapper
