#! /urs/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py consulta-sql 
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2023
# -----------------------------
import sys, os, signal, argparse

parser = argparser.ArgumentParser(description="Gestionar alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
print(args)
sys.exit(0)


def myhandler(signum,frame):
    print("Signal handler wth signal:", signum)
    print("hasta luego lucas")
    sys.exit(0)

def myalarm(signum,frame):


signal.signal(signal.SIGALRM,myhandler) #Quan rebi el signal, executa myhandler
signal.signal(signal.SIGUSR2,myhandler)
signal.signal(signal.SIGUSR1,mydeath)
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,signal.SIG_IGN)

signal.alarm(60)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
