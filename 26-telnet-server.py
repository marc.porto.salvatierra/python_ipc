# /usr/bin/python
#-*- coding: utf-8-*-
# cal-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------

import sys,socket, os, signal
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
       help=" port del server", default=50001)
parser.add_argument("-d", "--debug", action="store_true", help="trace de les accions")
args=parser.parse_args()

def senyal1(signum, frame):
    print("Lista de conexiones:")
    print(lista_conexiones)
    print("Vaciando lista de conexiones")
    lista_conexiones = []

def senyal12(signum, frame):
    print("Cantidad total de conexiones:")
    print(contador_conexiones)
    sys.exit(0)

signal.signal(signal.SIGUSR2, senyal12)
signal.signal(signal.SIGHUP, senyal1)

HOST = ''
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')
DEBUG = args.debug
# ---------------------------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
pid = os.fork()

if pid != 0:
    print("Terminando programa padre.")
    sys.exit(0)

lista_conexiones = []
contador_conexiones = 0

while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    lista_conexiones.append(addr)
    contador_conexiones += 1
    while True:
        command = conn.recv(1024)
        if DEBUG: print('Recive', repr(command))
        if not command: break
        pipeData = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        for line in pipeData.stderr:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        conn.sendall(MYEOF)
    conn.close()
s.close()
sys.exit(0)


