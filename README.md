## Scripts/Programació Python de Sistemes

IPC == Inter process Communication
	- Pipes, Signals, Sockets .[shared memory]
	- Crear Processos[fork]


argparse:

action="append" -> fer llistes
action="store_true" -> si se cumple, pasa a True



ordre -> stdin(pipe) -> stdout(pipe)
popen.stdout -> llegeix l'ordre fill(who) 

### 12-popen-sql.py
Hi ha un docker a dockerhub:
$ docker run --rm --name psql -h psql -it edtasixm06/postgres /bin/bash

A la adreça de github [asixm06-docker](https://github.com/edtasixm06/asixm06-docker/tree/master/postgres:base) hi la les ordres per engegar el postgres.

Cal fer-les per posar en marxa el servei i inicialitzar la base de dades.
$ su -l postgres
$ /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start

Verificar el funcionament des de dins del container:
$ psql -qtA -F',' training -c "select * from clientes;"


Des del host executar consultes, cal indicar la adreça ip del container al que connectem, i l’usuari (role) que és edtasixm06:
$  psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c "select * from clientes;"

Si volem que el container faci map a un dels ports del host:
$ docker run --rm --name psql -h psql -p 5432:5432  -it edtasixm06/postgres /bin/bash
$inicialitzar des de dins del docker
$ psql -qtA -F','  -h d02  -U edtasixm06 training -c "select * from oficinas;"


preparedStatement(sentencia,var,var...)


find /tmp -type s -ls 2> /dev/null

nc or ns

SOCK_STREAM // TCP
SOCK_DGRAM //UDP

echo -> 7
daytime -> 13
chargen -> 19


bind -> a quin port es connecta/adreça
HOST -> per quina if s'esta connectant

1- client connecta
2- accepta connexio
3- client fa informe
4- client "send" informe
5- server fa un bucle per cada informe rebut
6- tanca la connexio
7- if not data


tota connexio de sortida es dinamic el seu port 41.000
if not data -> si el de l'altre extrem tanca la connexio

