#! /urs/bin/python
#-*- coding: utf-8-*-
#
# head [ -n nlin] [-f file]...
# 10 lines, file o stdin
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2022
# -----------------------------

import sys
import argparse

parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="5|10|15", choices=[5,10,15], default=10)
parser.add_argument("-f","--file",type=str,\
        help="fitxer a processar", metavar="file",\
        dest="fileIn", action="append")
args=parser.parse_args()
print(args)

MAXLIN=args.nlin
if args.fileIn:
    for files in args.fileIn:
        files=open(files,"r")
        counter=0
        for line in files:
            counter+=1 
            print(line, end="") # end="" -> Serveix per que no faci salt de linea

            if counter==MAXLIN: break

        files.close()
exit(0)


