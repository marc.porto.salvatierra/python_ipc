# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal.py  segons
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,os, signal, argparse
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
mes = 0
menys = 0
def mysigusr1(signum,frame):
    global mes
    print("Signal handler called with signal:", signum)
    print("s'han sumat 60 segons")
    mes+=1
    actual = signal.alarm(0)
    signal.alarm(actual+60)
def mysigusr2(signum,frame):
    global menys
    print("Signal handler called with signal:", signum)
    print("S'han restat 60 segons")
    menys+=1
    actual=signal.alarm(0)
    if actual-60<0:
        print("ignored %d" % (actual))
        signal.alarm(actual)
    else:
        signal.alarm(actual-60)

def mysighup(signum,frame):
    print("Signal handler called with signal:", signum)
    print("reiniciant: ", args.segons)
    signal.alarm(args.segons)

def mysigterm(signum,frame):
    print("Signal handler called with signal:", signum)
    falta = signal.alarm(0)
    signal.alarm(falta)
    print("Falten %d segons" % (falta))

def mysigalarm(signum,frame):
    global mes, menys
    print("Signal handler called with signal:", signum)
    print("finalitzant... upp:%d down:%d" % (mes,menys))
    sys.exit(1)




signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGALRM,mysigalarm)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(args.segons)
print(os.getpid())

while True:
  pass
signal.alarm(0)
sys.exit(0)

