# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,os

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid!=0:
    print("Programa pare", os.getpid(), pid)
    sys.exit(0)

print("Programa fill", os.getpid(), pid)
#os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/"])
os.execl("/usr/bin/python3","/usr/bin/python3","16-signal.py","10")
#os.execlp("","ls","-lh","/opt")
#os.execvp("uname",["uname","-a"])
#os.execle("/bin/bash","/bin/bash","show.sh",{"nom":"joan","edat":"25"})

print("Hasta luego Lucas")
sys.exit(0)
