# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,socket

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT)) # es connecta
while True:
    data = s.recv(1024) # emmagatzema lo rebut
    if not data:
        break
    print('Received', repr(data))
s.close()
sys.exit(0)
