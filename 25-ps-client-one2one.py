# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,socket,argparse
from subprocess import PIPE, Popen
parser = argparse.ArgumentParser(description="")
parser.add_argument("-s", type=str,\
        help = "Servidor", dest= "server")
parser.add_argument("-p", type=int,\
        help ="Port", dest="port", default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT)) # es connecta
command = "ps ax"
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
    s.send(line)
s.close()
sys.exit(0)
