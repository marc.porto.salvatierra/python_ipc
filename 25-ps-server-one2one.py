# /usr/bin/python
#-*- coding: utf-8-*-
# cal-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket,os,signal,argparse,time
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
llista=[]
HOST=''
PORT=args.port
# ------------------------------------
def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def mysigterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)
  
pid=os.fork()
if pid !=0:
  print("Engegat el server CAL:", pid)
  sys.exit(0)
  
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  llista.append(addr)
  fileLog="~/m06/python_ipc/%s-%s-%s.txt" % (addr[0],addr[1],time.strftime("%Y%m%d-%H%M%s"))
  f=open(fileLog, "w")
  while True: # escolta el client
      data = conn.recv(1024)
      if not data: break
      f.write(str(data))
  conn.close()
  f.close()
sys.exit(0)


  


