# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,socket,argparse
parser = argparse.ArgumentParser(description="")
parser.add_argument("-s", type=str,\
        help = "Servidor", dest= "server", default="127.0.0.1")
parser.add_argument("-p", type=int,\
        help ="Port", dest="port", default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT)) # es connecta
while True:
    data = s.recv(1024) # emmagatzema lo rebut
    if not data:
        break
    print('Received', repr(data))
s.close()
sys.exit(0)
