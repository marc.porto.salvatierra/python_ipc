#! /urs/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py consulta-sql 
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2023
# -----------------------------

import sys
import argparse
from subprocess import Popen, PIPE
parser =  argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("consulta", type=str,\
        help="consulta sql")
args=parser.parse_args()
#----------------
command = [ f"psql -qtA -F',' -h localhost -U edtasixm06 training -c \"{args.consulta}\"" ]
pipeData = Popen(command,shell=True, stdout = PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit(0)
