#! /urs/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py consulta-sql 
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2023
# -----------------------------
import sys, os, signal

def myhandler(signum,frame):
    print("Signal handler wth signal:", signum)
    print("hasta luego lucas")

signal.signal(signal.SIGALRM,myhandler) #Quan rebi el signal, executa myhandler
signal.signal(signal.SIGUSR2,myhandler)
signal.signal(signal.SIGUSR1,myhandler)
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,signal.SIG_IGN)

signal.alarm(60)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
