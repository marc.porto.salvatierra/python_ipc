#! /urs/bin/python
#-*- coding: utf-8-*-
#
# head [file]
# 10 lines, file o stdin
#
# -----------------------------
# @marc.porto.salvatierra ASIX2
# Gener 2022
# -----------------------------

import argparse

parser = argparse.ArgumentParser(\
        description="Programa exemple arguments",\
        prog="02-arguments.py",\
        epilog="hasta luego lucas")
parser.add_argument("-e","--edat", type=int,\
        dest="useredat", help="edat a processar",\
        metavar="edat")
parser.add_argument("-n","--nom", type=str,\
        help="nom de usuari")
args=parser.parse_args()
print(args)
print(args.useredat, args.nom)

# a201532mp@g18:~/m06/python_ipc$ python3 02-exemple-args.py -e 26 -n marc
# Namespace(useredat=26, nom='marc')
# 26 marc

