# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,socket

HOST = ''
PORT = 7
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT)) # es connecta
s.send(b'Hello, world') # envia
data = s.recv(1024) # emmagatzema lo rebut
s.close()
print('Received', repr(data))
sys.exit(0)
