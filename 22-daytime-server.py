# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exec-signal.py 
# -------------------------------------
# @marc.porto.salvatierra ASIX M06 
# Gener 2023
# -------------------------------------
import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT)) # es connecta
s.listen(1)
conn, addr = s.accept() # accepta l'IP:port i la connexio
print("Conn:", type(conn), conn)
print("Connected: ", addr)
command = [ "date" ]
pipeData = Popen(command, stdout = PIPE)

for line in pipeData.stdout:
    conn.send(line)
conn.close()
sys.exit(0)
